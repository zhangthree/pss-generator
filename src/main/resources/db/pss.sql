/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2018-10-10 23:41:37                          */
/*==============================================================*/


drop table if exists customer;

drop table if exists department;

drop table if exists dictionary;

drop table if exists employees;

drop table if exists in_stock_detail;

drop table if exists in_stock_order;

drop table if exists menu;

drop table if exists out_stock_detail;

drop table if exists out_stock_order;

drop table if exists price;

drop table if exists product;

drop table if exists role;

drop table if exists role_menu_relationship;

drop table if exists stock;

drop table if exists stock_price_adjustment;

drop table if exists supplier;

drop table if exists user;

drop table if exists user_role_relationship;

drop table if exists warehouse;

/*==============================================================*/
/* Table: customer                                              */
/*==============================================================*/
create table customer
(
   id                   varchar(32) not null comment '唯一标识',
   code                 int not null comment '编号',
   name                 varchar(20) not null comment '姓名',
   level                int comment '级别',
   industry             varchar(40) comment '行业',
   phone                varchar(20) comment '手机',
   duty                 varchar(40) comment '职务',
   tax_name             varchar(50) comment '发票抬头',
   tax_number           varchar(20) comment '纳税人识别号',
   address              varchar(100) comment '发票地址',
   telephone            varchar(20) comment '发票座机',
   bank                 varchar(20) comment '开户行',
   account              varchar(20) comment '对公账户',
   legal_person         varchar(20) comment '法人',
   last_order_date      date comment '上次订货时间',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id),
   unique key AK_code (code)
);

alter table customer comment '客户信息';

/*==============================================================*/
/* Table: department                                            */
/*==============================================================*/
create table department
(
   id                   varchar(32) not null comment '唯一标识',
   code                 int comment '编号',
   name                 varchar(20) comment '名称',
   manager              int comment '经理',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id),
   unique key AK_code (code)
);

alter table department comment '部门信息';

/*==============================================================*/
/* Table: dictionary                                            */
/*==============================================================*/
create table dictionary
(
   id                   varchar(32) not null comment '唯一标识',
   code                 int not null comment '编号',
   name                 varchar(20) not null comment '字典名称',
   reference_table      varchar(40) not null comment '关联表',
   reference_column     varchar(40) comment '关联字段',
   value                varchar(20) comment '字典值',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id),
   unique key AK_code (code)
);

alter table dictionary comment '系统字典';

/*==============================================================*/
/* Table: employees                                             */
/*==============================================================*/
create table employees
(
   code                 int not null comment '工号',
   name                 varchar(20) not null comment '姓名',
   id_card              varchar(20) not null comment '身份证',
   gender               int comment '性别',
   phone                varchar(20) comment '手机',
   birthday             date comment '生日',
   age                  int comment '年龄',
   department           varchar(32) comment '部门',
   status               int default 1 comment '状态',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (code)
);

alter table employees comment '员工信息';

/*==============================================================*/
/* Table: in_stock_detail                                       */
/*==============================================================*/
create table in_stock_detail
(
   row_no               int not null comment '序号',
   order_no             varchar(15) not null comment '入库单号',
   product_code         int comment '商品编码',
   product_name         varchar(20) comment '商品名称',
   number               numeric(5,1) comment '数量',
   unit                 varchar(10) comment '单位',
   price                decimal(10,2) comment '价格',
   amount               decimal(10,2) comment '金额',
   batch_no             varchar(20) not null comment '批次',
   supplier_code        int comment '供货商编码',
   supplier_name        varchar(40) comment '供货商名称',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (row_no)
);

alter table in_stock_detail comment '入库详情';

/*==============================================================*/
/* Table: in_stock_order                                        */
/*==============================================================*/
create table in_stock_order
(
   order_no             varchar(15) not null comment '单据号',
   warehouse_code       int comment '仓库编号',
   warehouse_name       varchar(50) comment '仓库名字',
   type                 int comment '类型',
   status               int comment '状态',
   order_total          decimal(10,2) comment '单据总额',
   order_owner          int comment '填单人',
   owner_date           date comment '填单日期',
   order_auditor        int comment '入库人',
   auditor_date         date comment '入库日期',
   remarks              varchar(100) comment '备注',
   delete_flag          int not null default 0 comment '删除标志',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (order_no)
);

alter table in_stock_order comment '入库单据';

/*==============================================================*/
/* Table: menu                                                  */
/*==============================================================*/
create table menu
(
   id                   varchar(32) not null comment '唯一标识',
   code                 int not null comment '编号',
   name                 varchar(20) comment '名称',
   parent_id            int comment '上级编号',
   url                  varchar(100) comment '资源路径',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id),
   unique key AK_code (code)
);

alter table menu comment '菜单信息';

/*==============================================================*/
/* Table: out_stock_detail                                      */
/*==============================================================*/
create table out_stock_detail
(
   row_no               int not null comment '序号',
   order_no             varchar(15) not null comment '出库单号',
   product_code         int comment '商品编码',
   product_name         varchar(20) comment '商品名称',
   price                decimal(10,2) comment '价格',
   number               numeric(5,1) comment '数量',
   unit                 varchar(10) comment '单位',
   amount               decimal(10,2) comment '金额',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (row_no)
);

alter table out_stock_detail comment '出库详情';

/*==============================================================*/
/* Table: out_stock_order                                       */
/*==============================================================*/
create table out_stock_order
(
   order_no             varchar(15) not null comment '单据号',
   type                 int comment '类型',
   status               int comment '状态',
   warehouse_code       int comment '仓库编码',
   warehouse_name       varchar(50) comment '仓库名称',
   customer_code        int comment '客户编码',
   customer_name        varchar(20) comment '客户姓名',
   order_total          decimal(10,2) comment '单据总额',
   order_owner          int comment '填单人',
   owner_date           date comment '填单日期',
   order_auditor        int comment '出库人',
   auditor_date         date comment '出库日期',
   remarks              varchar(100) comment '备注',
   delete_flag          int not null default 0 comment '删除标志',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (order_no)
);

alter table out_stock_order comment '出库单据';

/*==============================================================*/
/* Table: price                                                 */
/*==============================================================*/
create table price
(
   id                   varchar(32) not null comment '唯一标识',
   product_id           varchar(32) comment '商品标识',
   supplier_id          varchar(32) comment '供货商标识',
   batch_no             varchar(20) comment '采购批次',
   purchase_price       decimal(10,2) comment '采购价',
   sales_price          decimal(10,2) comment '销售价',
   status               int not null comment '状态',
   rater                int comment '定价人',
   price_date           date comment '定价日期',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id)
);

alter table price comment '价格信息';

/*==============================================================*/
/* Table: product                                               */
/*==============================================================*/
create table product
(
   id                   varchar(32) not null comment '唯一标识',
   code                 int not null comment '编码',
   name                 varchar(20) not null comment '名称',
   status               int not null default 1 comment '状态',
   type                 int comment '类别',
   brand                int comment '品牌',
   min_unit             varchar(10) comment '最小单位',
   max_unit             varchar(10) comment '最大单位',
   unit_ratio           int comment '单位换算比率',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id),
   unique key AK_code (code)
);

alter table product comment '商品信息';

/*==============================================================*/
/* Table: role                                                  */
/*==============================================================*/
create table role
(
   id                   varchar(32) not null comment '唯一标识',
   code                 int not null comment '编号',
   name                 varchar(20) comment '名称',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id),
   unique key AK_code (code)
);

alter table role comment '角色信息';

/*==============================================================*/
/* Table: role_menu_relationship                                */
/*==============================================================*/
create table role_menu_relationship
(
   id                   varchar(32) not null comment '唯一标识',
   code                 int comment '编号',
   role_id              varchar(32) not null comment '角色标识',
   menu_id              varchar(32) not null comment '菜单标识',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id),
   unique key AK_code (code)
);

alter table role_menu_relationship comment '角色菜单关系';

/*==============================================================*/
/* Table: stock                                                 */
/*==============================================================*/
create table stock
(
   id                   varchar(32) not null comment '唯一标识',
   product_id           varchar(32) comment '商品标识',
   product_code         int comment '商品编码',
   product_name         varchar(20) comment '商品名称',
   purchase_price       decimal(10,2) comment '采购价',
   sales_price          decimal(10,2) comment '销售价',
   number               numeric(5,1) comment '数量',
   batch_no             varchar(20) comment '批次',
   supplier_code        int comment '供货商编码',
   supplier_name        varchar(40) comment '供货商名称',
   warehouse_code       int comment '仓库编码',
   warehouse_name       varchar(50) comment '仓库名称',
   delete_flag          int not null default 0 comment '删除标志',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id)
);

alter table stock comment '库存信息';

/*==============================================================*/
/* Table: stock_price_adjustment                                */
/*==============================================================*/
create table stock_price_adjustment
(
   order_no             varchar(15) not null comment '单据号',
   product_id           varchar(32) comment '商品标识',
   product_code         int comment '商品编码',
   product_name         varchar(20) comment '商品名称',
   number               numeric(5,1) comment '数量',
   unit                 varchar(10) comment '单位',
   warehouse_code       int comment '仓库编号',
   warehouse_name       varchar(50) comment '仓库名称',
   old_batch_no         varchar(20) comment '原批次',
   old_purchase_price   decimal(10,2) comment '原采购价',
   old_sales_price      decimal(10,2) comment '原销售价',
   old_purchase_amount  decimal(10,2) comment '原采购金额',
   old_sales_amount     decimal(10,2) comment '原销售金额',
   new_batch_no         varchar(20) comment '现批次',
   new_purchase_price   decimal(10,2) comment '现采购价',
   new_sales_price      decimal(10,2) comment '现销售价',
   new_purchase_amount  decimal(10,2) comment '现采购金额',
   new_sales_amount     decimal(10,2) comment '现销售金额',
   purchase_difference  decimal(10,2) comment '采购差额',
   sales_difference     decimal(10,2) comment '销售差额',
   order_owner          int comment '操作人',
   owner_date           date comment '操作日期',
   delete_flag          int not null default 0 comment '删除标识',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_date          timestamp not null comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (order_no)
);

alter table stock_price_adjustment comment '库存商品调价';

/*==============================================================*/
/* Table: supplier                                              */
/*==============================================================*/
create table supplier
(
   id                   varchar(32) not null comment '唯一标识',
   code                 int not null comment '编码',
   name                 varchar(40) not null comment '名称',
   telephone            varchar(20) comment '电话',
   contacts             varchar(20) comment '联系人',
   address              varchar(100) comment '地址',
   legal_person         varchar(40) comment '法人',
   tax_number           varchar(20) comment '纳税人识别号',
   bank                 varchar(20) comment '开户行',
   account              varchar(20) comment '对公账户',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id),
   unique key AK_code (code)
);

alter table supplier comment '供货商信息';

/*==============================================================*/
/* Table: user                                                  */
/*==============================================================*/
create table user
(
   id                   varchar(32) not null comment '唯一标识',
   usercode             int not null comment '用户名',
   password             varchar(20) default '123456' comment '密码',
   login_time           timestamp comment '登录时间',
   logout_time          timestamp comment '注销时间',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id)
);

alter table user comment '用户信息';

/*==============================================================*/
/* Table: user_role_relationship                                */
/*==============================================================*/
create table user_role_relationship
(
   id                   varchar(32) not null comment '唯一标识',
   code                 int comment '编号',
   user_id              varchar(32) not null comment '用户标识',
   role_id              varchar(32) not null comment '角色标识',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id),
   unique key AK_code (code)
);

alter table user_role_relationship comment '用户角色关系';

/*==============================================================*/
/* Table: warehouse                                             */
/*==============================================================*/
create table warehouse
(
   id                   varchar(32) not null comment '唯一标识',
   code                 int not null comment '编号',
   name                 varchar(50) not null comment '名称',
   manager              varchar(20) comment '负责人',
   phone                varchar(20) comment '电话',
   address              varchar(100) comment '地址',
   insert_time          timestamp not null DEFAULT CURRENT_TIMESTAMP comment '录入时间',
   insert_by            int not null comment '录入人',
   update_time          timestamp not null DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新时间',
   update_by            int not null comment '更新人',
   primary key (id),
   unique key AK_code (code)
);

alter table warehouse comment '仓库信息';

alter table employees add constraint FK_employees_department foreign key (department)
      references department (id) on delete restrict on update restrict;

alter table in_stock_detail add constraint FK_inStockOrder_order_no foreign key (order_no)
      references in_stock_order (order_no) on delete CASCADE on update restrict;

alter table out_stock_detail add constraint FK_outStockOrder_order_no foreign key (order_no)
      references out_stock_order (order_no) on delete cascade on update restrict;

alter table price add constraint FK_product_price foreign key (product_id)
      references product (id) on delete restrict on update restrict;

alter table price add constraint FK_supplier_price foreign key (supplier_id)
      references supplier (id) on delete restrict on update restrict;

alter table role_menu_relationship add constraint FK_roleMenuRelationship_menu_id foreign key (menu_id)
      references menu (id) on delete restrict on update restrict;

alter table role_menu_relationship add constraint FK_role_role_id foreign key (role_id)
      references role (id) on delete restrict on update restrict;

alter table user add constraint FK_employees_usercode foreign key (usercode)
      references employees (code) on delete restrict on update restrict;

alter table user_role_relationship add constraint FK_userRoleRelationship_role_id foreign key (role_id)
      references role (id) on delete restrict on update restrict;

alter table user_role_relationship add constraint FK_userRoleRelationship_user_id foreign key (user_id)
      references user (id) on delete restrict on update restrict;

