package me.zhangsanfeng.genenator;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import me.zhangsanfeng.frame.entity.BaseEntity;

import java.util.List;

/**
 * @ClassName:CodeGenenator
 * @Description: mybatis plus代码生成器
 * @Author:ZhangYao
 * @Date:2018/10/12 02:21
 * @Version:1.0
 */
public class CodeGenenator {
    final static String packagePath = "D:/Develop-Kit/document";
    final static String resourcesPath = "/Users/Yao/Development-Kit/Project/pss/src/main/resources/";
    final static String packageName = "me.zhangsanfeng";
    final static String moduleName = "base";
    static String[] tables = {"product","supplier","warehouse","price","employees","department","customer"};
    static String[] superEntityColumns = {"insert_time","insert_by","update_time","update_by"};

    public static void main(String[] args){
        AutoGenerator generator = new AutoGenerator();

        //全局配置
        GlobalConfig globalConfig = new GlobalConfig();
//        globalConfig.setOutputDir("/Users/Yao/Development-Kit/Doc/pss");
        globalConfig.setOutputDir(packagePath);
        globalConfig.setActiveRecord(false);
        globalConfig.setFileOverride(true);
        globalConfig.setAuthor("ZhangYao");
        globalConfig.setSwagger2(true);
        globalConfig.setBaseResultMap(true);
        globalConfig.setBaseColumnList(true);
        globalConfig.setDateType(DateType.SQL_PACK);
        //globalConfig.setEntityName("%sEntity");
        globalConfig.setMapperName("%sDao");
        globalConfig.setXmlName("%sMapper");
        globalConfig.setServiceName("%sService");
        globalConfig.setServiceImplName("%sServiceImpl");
        globalConfig.setControllerName("%sController");
        globalConfig.setIdType(IdType.UUID);
        generator.setGlobalConfig(globalConfig);

        //数据源配置
        DataSourceConfig dataSource = new DataSourceConfig();
        dataSource.setDbType(DbType.MYSQL);
        dataSource.setUrl("jdbc:mysql://127.0.0.1:3306/pss?useUnicode=true&characterEncoding=UTF-8&allowMultiQueries=true");
        dataSource.setDriverName("com.mysql.jdbc.Driver");
        dataSource.setUsername("root");
        dataSource.setPassword("123456");
        generator.setDataSource(dataSource);

        //包配置
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.setParent(packageName);
        packageConfig.setModuleName(moduleName);
        packageConfig.setMapper("dao");
        packageConfig.setXml("mapper");
        generator.setPackageInfo(packageConfig);

        //自定义配置（自定义生成路径）
        InjectionConfig injection = new InjectionConfig(){
            @Override
            public void initMap() {

            }

            @Override
            public InjectionConfig setFileOutConfigList(List<FileOutConfig> fileOutConfigList) {
                fileOutConfigList.add(new FileOutConfig() {
                    @Override
                    public String outputFile(TableInfo tableInfo) {
                        return resourcesPath + "mapper/" +tableInfo.getEntityName() + "Mapper.xml";
                    }
                });
                return super.setFileOutConfigList(fileOutConfigList);
            }
        };
        generator.setCfg(injection);

        //策略配置
        StrategyConfig strategyConfig = new StrategyConfig();
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setEntityLombokModel(true);
        strategyConfig.setRestControllerStyle(true);
        strategyConfig.setControllerMappingHyphenStyle(false);
        strategyConfig.setLogicDeleteFieldName("delete_flag");
        strategyConfig.setSuperEntityClass("me.zhangsanfeng.frame.entity.BaseEntity");
        strategyConfig.setSuperEntityColumns(superEntityColumns);
//        strategyConfig.setSuperControllerClass("me.zhangsanfeng.pss.genenator.BaseController");
//        strategyConfig.setSuperServiceClass("me.zhangsanfeng.pss.genenator.BaseService");
//        strategyConfig.setSuperMapperClass("me.zhangsanfeng.pss.genenator.BaseDao");
        strategyConfig.setInclude(tables);
        generator.setStrategy(strategyConfig);

        generator.execute();
    }
}
