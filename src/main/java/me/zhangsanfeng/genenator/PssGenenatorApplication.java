package me.zhangsanfeng.genenator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PssGenenatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(PssGenenatorApplication.class, args);
    }
}
